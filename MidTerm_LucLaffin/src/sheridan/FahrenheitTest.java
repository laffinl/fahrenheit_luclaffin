package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/*
 * LUC LAFFIN 991389277
 */

public class FahrenheitTest {

	@Test
	public void testFromCelsiusRegualr() {
		assertTrue("Invalid entry", Fahrenheit.fromCelsius(0) == 32);
	}
	
	@Test 
	public void testFromCelsiusException() {
		assertFalse("Invalid entry", Fahrenheit.fromCelsius(100) == 31);
	}
	
	@Test
	public void testFromCelsiusBoundaryIn() {
		assertTrue("Invalid entry", Fahrenheit.fromCelsius(1) == 34 );
		//should round 145.4 down to 145
		
	}
	
	@Test
	public void testFromCelsiusBoundaryOut() {
		assertTrue("Invalid entry", Fahrenheit.fromCelsius(-1) == 30);
		//should round 150.8 to 151
		
	}
	
	

}
