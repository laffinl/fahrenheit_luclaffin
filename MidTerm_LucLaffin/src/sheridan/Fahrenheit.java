package sheridan;

public class Fahrenheit  {
	
	public static int fromCelsius(int temp) {
		
		return (int) Math.round((temp * (9.0/5) + 32));
	}

}
